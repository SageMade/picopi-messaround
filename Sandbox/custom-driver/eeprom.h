#pragma once
#include "hardware/i2c.h"

class EEPROM {
    public:
        EEPROM(i2c_inst_t* i2c, uint8_t devId = 0x50);

        void write(uint16_t address, const void* data, size_t len);
        void read(uint16_t address, void* data, size_t len);
        
        template <typename T>
        void read(uint16_t address, T& output) {
            read(address, &output, sizeof(T));
        }

        template <typename T>
        void write(uint16_t address, const T& value) {
            write(address, &value, sizeof(T));
        }

    protected:
        i2c_inst_t* i2c_;
        uint8_t devId_;
        uint8_t workingBuffer_[66];
};