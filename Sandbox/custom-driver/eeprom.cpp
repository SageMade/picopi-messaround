#include "eeprom.h"
#include <cstring>


EEPROM::EEPROM(i2c_inst_t* i2c, uint8_t devId) :
    i2c_(i2c),
    devId_(devId)
{ }

void EEPROM::write(uint16_t address, const void* data, size_t len) {
    for(uint16_t ix = 0; ix < len; ix += 64) {
        uint16_t toWrite = (len - ix) < 64 ? (len - ix) : 64;
        uint8_t commandBuffer[] = {
            (uint8_t)((address + ix) >> 8),
            (uint8_t)((address + ix) & 0xFF)
        };
        workingBuffer_[0] = (uint8_t)((address + ix) >> 8);
        workingBuffer_[1] = (uint8_t)((address + ix) & 0xFF);
        memcpy(workingBuffer_ + 2, (uint8_t*)data + ix, toWrite);
        i2c_write_blocking(i2c_, devId_, workingBuffer_, 2 + toWrite, false);
    }
}
void EEPROM::read(uint16_t address, void* data, size_t len) {
        uint8_t commandBuffer[] = {
            (uint8_t)(address >> 8),
            (uint8_t)(address & 0xFF)
        };
        i2c_write_blocking(i2c_, devId_, commandBuffer, 2, true);
        i2c_read_blocking(i2c_, devId_, (uint8_t*)data, len, false);
}