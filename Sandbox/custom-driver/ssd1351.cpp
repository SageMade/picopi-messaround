#include "ssd1351.h"

Ssd1351::Ssd1351(spi_inst_t* spi, const Ssd1351Config& config) {
    spi_ = spi;
    config_ = config;
    initialized_ = false;

    init();   
}

Ssd1351::~Ssd1351() {

}

void Ssd1351::enable() {
    _send_command(SSD1351_CMD_DISPLAYON);
}

void Ssd1351::disable() {
    _send_command(SSD1351_CMD_DISPLAYOFF);
}

void Ssd1351::setContrast(uint8_t contrast) {
    _send_command(SSD1351_CMD_CONTRASTMASTER);
    _send_data(contrast);
}

void Ssd1351::update() {
  _send_command(SSD1351_CMD_WRITERAM);
  _send_data(frameBuffer_->bytes, frameBuffer_->sizeBytes);
}

Graphics& Ssd1351::getGraphics() {
    return *graphics_;
}

void Ssd1351::init() {
    if (config_.dc_pin > -1) {        
        gpio_init(config_.dc_pin);
        gpio_set_dir(config_.dc_pin, GPIO_OUT);
    }
    if (config_.cs_pin > -1) {        
        gpio_init(config_.cs_pin);
        gpio_set_dir(config_.cs_pin, GPIO_OUT);
    }
    if (config_.rst_pin > -1) {        
        gpio_init(config_.rst_pin);
        gpio_set_dir(config_.rst_pin, GPIO_OUT);
    }
    
    gpio_put(25, false);
    sleep_ms(500);
    gpio_put(25, true);
    sleep_ms(500);

    frameBuffer_ = nullptr;

    if (config_.sharedRAM != nullptr) {
        if (config_.sharedRAM->Width >= config_.width && config_.sharedRAM->Height >= config_.height) {
            frameBuffer_ = config_.sharedRAM;
        }
    }

    if (frameBuffer_ == nullptr) {
        frameBuffer_ = new DisplayRAM(config_.width, config_.height);
    }
    
    gpio_put(25, false);
    sleep_ms(500);
    gpio_put(25, true);

    graphics_ = new Graphics(config_.width, config_.height, frameBuffer_->halfw);
    
    gpio_put(25, false);
    sleep_ms(100);
    gpio_put(25, true);

    
    gpio_put(config_.rst_pin, 1);
    sleep_ms(60);
    gpio_put(config_.rst_pin, 0);
    sleep_ms(60);
    gpio_put(config_.rst_pin, 1);
    sleep_ms(60);
        
    _send_command(SSD1351_CMD_COMMANDLOCK);
    _send_data(0x12);
    _send_command(SSD1351_CMD_COMMANDLOCK);
    _send_data(0xB1);

    _send_command(SSD1351_CMD_DISPLAYOFF);
    _send_command(SSD1351_CMD_CLOCKDIV);
    _send_data(0xF1);
    
    // Reset MUX ratio
    _send_command(SSD1351_CMD_MUXRATIO);
    _send_data(127);
    
    // Wait for changes to apply
    sleep_ms(50);

    _send_command(SSD1351_CMD_SETREMAP);
    _send_data(0x20);

    _send_command(SSD1351_CMD_SETCOLUMN);
    _send_data({ 0x00, 0x7F });

    _send_command(SSD1351_CMD_SETROW);
    _send_data({ 0x00, 0x7F });

    _send_command(SSD1351_CMD_STARTLINE);
    _send_data(0x00);
    
    _send_command(SSD1351_CMD_DISPLAYOFFSET);
    _send_data(0x00);
    
    _send_command(SSD1351_CMD_SETGPIO);
    _send_data(0x00);
    
    _send_command(SSD1351_CMD_FUNCTIONSELECT);
    _send_data(0x01);
    
    _send_command(SSD1351_CMD_PRECHARGE);
    _send_data(0x32);
    
    _send_command(SSD1351_CMD_VCOMH);
    _send_data(0x05);
    
    _send_command(SSD1351_CMD_NORMALDISPLAY);
    
    _send_command(SSD1351_CMD_CONTRASTABC);
    _send_data( { 0x8A, 0x51, 0x8A } );
    
    _send_command(SSD1351_CMD_CONTRASTMASTER);
    _send_data(0x0F);
    
    _send_command(SSD1351_CMD_SETVSL);
    _send_data( { 0xA0, 0xB5, 0x55 } );
    
    _send_command(SSD1351_CMD_PRECHARGE2);
    _send_data(0x01);
    
    gpio_put(25, false);
    sleep_ms(100);
    gpio_put(25, true);

    update();
    
    _send_command(SSD1351_CMD_DISPLAYON);
    
    gpio_put(25, false);
    sleep_ms(100);
    gpio_put(25, true);

    initialized_ = true;
}

void Ssd1351::_send_command(uint8_t cmd) {
    gpio_put(config_.dc_pin, 0);
    gpio_put(config_.cs_pin, 0);
    spi_write_blocking(spi_, &cmd, 1);
    gpio_put(config_.cs_pin, 1);
}

void Ssd1351::_send_data(uint8_t value) {
    gpio_put(config_.dc_pin, 1);
    gpio_put(config_.cs_pin, 0);
    spi_write_blocking(spi_, &value, 1);
    gpio_put(config_.cs_pin, 1);
}

void Ssd1351::_send_data(const uint8_t* values, size_t count) {
    gpio_put(config_.dc_pin, 1);
    gpio_put(config_.cs_pin, 0);
    spi_write_blocking(spi_, values, count);
    gpio_put(config_.cs_pin, 1);
}

void Ssd1351::_send_data(const std::initializer_list<uint8_t>& values) {
    gpio_put(config_.dc_pin, 1);
    gpio_put(config_.cs_pin, 0);
    spi_write_blocking(spi_, values.begin(), values.size());
    gpio_put(config_.cs_pin, 1);
}