#pragma once

#include <initializer_list>
extern "C" {
    #include "pico/stdlib.h"
};
#include "hardware/spi.h"

#include "IttyMath.hpp"
#include "graphics.h"
#include <cstring>

#define SSD1351_CMD_SETCOLUMN       0x15
#define SSD1351_CMD_SETROW          0x75
#define SSD1351_CMD_WRITERAM        0x5C
#define SSD1351_CMD_READRAM         0x5D
#define SSD1351_CMD_SETREMAP        0xA0
#define SSD1351_CMD_STARTLINE       0xA1
#define SSD1351_CMD_DISPLAYOFFSET   0xA2
#define SSD1351_CMD_DISPLAYALLOFF   0xA4
#define SSD1351_CMD_DISPLAYALLON    0xA5
#define SSD1351_CMD_NORMALDISPLAY   0xA6
#define SSD1351_CMD_INVERTDISPLAY   0xA7
#define SSD1351_CMD_FUNCTIONSELECT  0xAB
#define SSD1351_CMD_DISPLAYOFF      0xAE
#define SSD1351_CMD_DISPLAYON       0xAF
#define SSD1351_CMD_PRECHARGE       0xB1
#define SSD1351_CMD_DISPLAYENHANCE  0xB2
#define SSD1351_CMD_CLOCKDIV        0xB3
#define SSD1351_CMD_SETVSL          0xB4
#define SSD1351_CMD_SETGPIO         0xB5
#define SSD1351_CMD_PRECHARGE2      0xB6
#define SSD1351_CMD_SETGRAY         0xB8
#define SSD1351_CMD_USELUT          0xB9
#define SSD1351_CMD_PRECHARGELEVEL  0xBB
#define SSD1351_CMD_VCOMH           0xBE
#define SSD1351_CMD_CONTRASTABC     0xC1
#define SSD1351_CMD_CONTRASTMASTER  0xC7
#define SSD1351_CMD_MUXRATIO        0xCA
#define SSD1351_CMD_COMMANDLOCK     0xFD
#define SSD1351_CMD_HORIZSCROLL     0x96
#define SSD1351_CMD_STOPSCROLL      0x9E
#define SSD1351_CMD_STARTSCROLL     0x9F

struct DisplayRAM {
    union {
        uint8_t* bytes;
        uint16_t* halfw;
    };
    const uint16_t Width;
    const uint16_t Height;
    const uint16_t sizeBytes;

    DisplayRAM(uint16_t w, uint16_t h) :
        Width(w), Height(h), sizeBytes(w * h * 2)
    {

        bytes = reinterpret_cast<uint8_t*>(malloc(sizeBytes));
        memset(bytes, 0, sizeBytes);
    }
    DisplayRAM(DisplayRAM&& other) :
        Width(other.Width), Height(other.Height), sizeBytes(other.sizeBytes)
    {
        bytes = other.bytes;
        other.bytes = nullptr;
    }
    ~DisplayRAM() {
        if (bytes != nullptr) {
            free(bytes);
            bytes = nullptr;
        }
    }

    uint16_t& operator()(uint16_t x, uint16_t y) {
        return *(halfw + ((Width - 1 - x) + (y * Width)));
    }
};

struct Ssd1351Config {
    int cs_pin;
    int dc_pin;
    int rst_pin;

    uint8_t rotation = 0;
    uint    width  = 128;
    uint    height  = 128;

    DisplayRAM* sharedRAM = nullptr;

    Ssd1351Config() :
        cs_pin(-1),
        dc_pin(-1),
        rst_pin(-1),
        rotation(0),
        width(128),
        height(128),
        sharedRAM(nullptr) 
    { }

};

class Ssd1351 {
    public:
        Ssd1351(spi_inst_t* spi, const Ssd1351Config& config);
        ~Ssd1351();

        void enable();
        void disable();
        void setContrast(uint8_t contrast);

        void update();

        Graphics& getGraphics();

    private:
        spi_inst_t*   spi_;
        Ssd1351Config config_;
        Graphics*     graphics_;
        bool          initialized_;

        DisplayRAM*   frameBuffer_;

        vec<2, uint16_t>      cursor_;
        rectangle_t<uint16_t> scissorRect_;

        void init();
        
        void _send_command(uint8_t cmd);
        void _send_data(uint8_t value);
        void _send_data(const uint8_t* values, size_t count);
        void _send_data(const std::initializer_list<uint8_t>& values);
};