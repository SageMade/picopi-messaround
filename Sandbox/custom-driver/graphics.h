#pragma once

#include <cstdint>
#include "IttyMath.hpp"
#include "fonts.h"

// Some color definitions
#define COLOR_BLUE 0x00F8
#define COLOR_RED 0x1F00
#define COLOR_GREEN 0xE007
#define COLOR_YELLOW 0xFF07
#define COLOR_PURPLE 0x1FF8
#define COLOR_AQUA 0xE0FF
#define COLOR_BLACK 0x0000
#define COLOR_WHITE 0xFFFF

class Graphics {
    public:
        Graphics(uint16_t w, uint16_t h, uint16_t* colBuff);

        void set_scissor_rect(const rectangle_t<uint16_t>& bounds);
        void set_scissor_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h);
        void reset_scissor_rect();

        void clear(uint16_t color);

        void draw_line(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);
        void draw_line(const vec<2, int16_t>& p1, const vec<2, int16_t>& p2, uint16_t color);

        void draw_rect(const rectangle_t<int16_t>& bounds, uint16_t color);
        void draw_rect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t color);
        
        void fill_rect(const rectangle_t<int16_t>& bounds, uint16_t color);
        void fill_rect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t color);
        
        void draw_circle(const vec<2, int16_t>& pos, uint16_t r, uint16_t color);
        void draw_circle(int16_t x, int16_t y, uint16_t r, uint16_t color);
        
        void fill_circle(const vec<2, int16_t>& pos, uint16_t r, uint16_t color);
        void fill_circle(int16_t x, int16_t y, uint16_t r, uint16_t color);

        void set_cursor(int16_t x, int16_t y);
        const vec<2, int16_t>& get_cursor() const;

        void print_char(font_t* font, int16_t& x, int16_t& y, uint16_t color, char c);
        void print_string(font_t* font, int16_t& x, int16_t& y, uint16_t color, const char* s);
        void printf(font_t* font, int16_t& x, int16_t& y, uint16_t color, const char* fmt, ...);
        
        void print_char(font_t* font, uint16_t color, char c);
        void print_string(font_t* font, uint16_t color, const char* s);
        void printf(font_t* font, uint16_t color, const char* fmt, ...);

        void set_default_font(font_t* font);

    private:
        uint16_t w_;
        uint16_t h_;
        uint16_t* colBuff_;
        font_t* defaultFont_;

        vec<2, int16_t> cursor_;

        rectangle_t<uint16_t> scissor_;
        rectangle_t<uint16_t> bounds_;

        void _set_pixel(int16_t x, int16_t y, uint16_t col);
        void _draw_circle_midpoints(int16_t cX, int16_t cY, int16_t x, int16_t y, uint16_t col);
        void _fill_circle_midpoints(int16_t cX, int16_t cY, int16_t x, int16_t y, uint16_t col);
        
        void _draw_line_low(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t col);
        void _draw_line_high(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t col);
};