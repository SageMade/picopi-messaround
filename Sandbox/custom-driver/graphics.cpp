#include "graphics.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <stdarg.h>

Graphics::Graphics(uint16_t w, uint16_t h, uint16_t* colBuff) :
    w_(w), h_(h), 
    colBuff_(colBuff),
    bounds_(0, 0, w, h), 
    scissor_(0, 0, w, h),
    cursor_(0, 0),
    defaultFont_(&Font_7x10)
{ }

void Graphics::set_scissor_rect(const rectangle_t<uint16_t>& bounds) {
    scissor_ = bounds_.Overlap(bounds);
}
void Graphics::set_scissor_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
    scissor_ = bounds_.Overlap(rectangle_t<uint16_t>(x, y, w, h));
}
void Graphics::reset_scissor_rect() {
    scissor_ = bounds_;
}

void Graphics::clear(uint16_t color) {
    for(uint16_t ix =0; ix < scissor_.Width(); ix++) {
        for(uint16_t iy =0; iy < scissor_.Height(); iy++) {
            _set_pixel(scissor_.minX + ix, scissor_.minY + iy, color);
        }
    }
}

void Graphics::draw_line(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color) {
    if (abs(y1 - y0) < abs(x1 - x0)){
        if (x0 > x1){
            _draw_line_low(x1, y1, x0, y0, color);
        }
        else{
            _draw_line_low(x0, y0, x1, y1, color);
        }
    }
    else{
        if (y0 > y1){
            _draw_line_high(x1, y1, x0, y0, color);
        }
        else{
            _draw_line_high(x0, y0, x1, y1, color);
        }
    }
}
void Graphics::draw_line(const vec<2, int16_t>& p1, const vec<2, int16_t>& p2, uint16_t color) {
    draw_line(p1.X, p1.Y, p2.X, p2.Y, color);
}

void Graphics::draw_rect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t color) {
    draw_line(x, y, x + w, y, color);         // ---
    draw_line(x, y, x, y + h, color);         // |
    draw_line(x + w, y, x + w, y + h, color); //    |
    draw_line(x, y + h, x + w, y + h, color); // ____
}
void Graphics::draw_rect(const rectangle_t<int16_t>& bounds, uint16_t color) {
    draw_rect(bounds.minX, bounds.minY, bounds.Width(), bounds.Height(), color);
}

void Graphics::fill_rect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t color) {
    for (int16_t iy = 0; iy <= h; iy++) {
        draw_line(x, y + iy, x + w, y + iy, color);
    }
}
void Graphics::fill_rect(const rectangle_t<int16_t>& bounds, uint16_t color) {
    fill_rect(bounds.minX, bounds.minY, bounds.Width(), bounds.Height(), color);
}

void Graphics::draw_circle(int16_t x, int16_t y, uint16_t r, uint16_t color) {
    int xM = 0, yM = r;
    int d = 3 - 2 * r;
    _draw_circle_midpoints(x, y, xM, yM, color);
    while (yM >= xM){
    xM++;
    if (d > 0){
        yM--;
        d = d + 4 * (xM - yM) + 10;
    }
    else{
        d = d + 4 * xM + 6;
    }
    _draw_circle_midpoints(x, y, xM, yM, color);
    }
}
void Graphics::draw_circle(const vec<2, int16_t>& pos, uint16_t r, uint16_t color) {
    draw_circle(pos.X, pos.Y, r, color);
}

void Graphics::fill_circle(int16_t x, int16_t y, uint16_t r, uint16_t color) {
    int xM = 0, yM = r;
    int d = 3 - 2 * r;
    _fill_circle_midpoints(x, y, xM, yM, color);
    while (yM >= xM){
        xM++;
        if (d > 0){
            yM--;
            d = d + 4 * (xM - yM) + 10;
        }
        else{
            d = d + 4 * xM + 6;
        }
        _fill_circle_midpoints(x, y, xM, yM, color);
    }
}
void Graphics::fill_circle(const vec<2, int16_t>& pos, uint16_t r, uint16_t color) {
    fill_circle(pos.X, pos.Y, r, color);
}

void Graphics::set_cursor(int16_t x, int16_t y) {
    cursor_.x = x > bounds_.maxX ? bounds_.maxX : x;
    cursor_.y = y > bounds_.maxY ? bounds_.maxY : y;
}
const vec<2, int16_t>& Graphics::get_cursor() const {
    return cursor_;
}

void Graphics::print_char(font_t* font, int16_t& x, int16_t& y, uint16_t color, char c) {
    uint16_t fd;
    if ((w_ <= x + font->width) || (w_ <= y + font->height)){
        return;
    }
    if (c == '\n'){
        x = 127;
    }
    else{
    for (int i = 0; i < font->height; i++){
        fd = font->data[(c - 32) * font->height + i];
        for (int j = 0; j < font->width; j++){
            if ((fd << j) & 0x8000){
                _set_pixel(x + j, y + i, color);
            }
        }
        }
    }
    x += font->width;
    if ((x + font->width >= w_ - 1) & (y + font->height <= h_ - 1)){
        y = y + font->height + 2;
        x = 0;
    }
    return;
}
void Graphics::print_string(font_t* font, int16_t& x, int16_t& y, uint16_t color, const char* s) {
    if (s == nullptr) return;
    
    while (*s != 0){
        print_char(font, x, y, color, *s);
        s++;
    }
}
void Graphics::printf(font_t* font, int16_t& x, int16_t& y, uint16_t color, const char* fmt, ...) {
    if (fmt == nullptr) {
        return;
    }
    va_list valist;
    va_start(valist, fmt);

    static char buffer[256];
    vsnprintf(buffer, 256, fmt, valist);
    print_string(font, x, y, color, buffer);
}

void Graphics::print_char(font_t* font, uint16_t color, char c) {
    print_char(font, cursor_.x, cursor_.y, color, c);
}
void Graphics::print_string(font_t* font, uint16_t color, const char* s) {
    print_string(font, cursor_.x, cursor_.y, color, s);
}
void Graphics::printf(font_t* font, uint16_t color, const char* fmt, ...) {
    if (fmt == nullptr) {
        return;
    }
    va_list valist;
    va_start(valist, fmt);

    static char buffer[256];
    vsnprintf(buffer, 256, fmt, valist);
    print_string(font, cursor_.x, cursor_.y, color, buffer);
}

void Graphics::set_default_font(font_t* font) {
    if (font != nullptr) {
        defaultFont_ = font;
    }
}

void Graphics::_set_pixel(int16_t x, int16_t y, uint16_t col) {
    if ( x > w_ - 1 || y > h_ -1 || x < 0 || y < 0) { 
        return;
    }
    if (scissor_.Contains(x, y)) {
        colBuff_[(w_ - 1 - x) + (w_ * y)] = col;
    }
}

void Graphics::_draw_circle_midpoints(int16_t cX, int16_t cY, int16_t x, int16_t y, uint16_t col) {
    _set_pixel(cX + x, cY + y, col);
    _set_pixel(cX - x, cY + y, col);
    _set_pixel(cX + x, cY - y, col);
    _set_pixel(cX - x, cY - y, col);
    _set_pixel(cX + y, cY + x, col);
    _set_pixel(cX - y, cY + x, col);
    _set_pixel(cX + y, cY - x, col);
    _set_pixel(cX - y, cY - x, col);
}

void Graphics::_fill_circle_midpoints(int16_t cX, int16_t cY, int16_t x, int16_t y, uint16_t col) {
    draw_line(cX - x, cY + y, cX + x, cY + y, col);
    draw_line(cX - x, cY - y, cX + x, cY - y, col);
    draw_line(cX -y,  cY + x,  cX + y, cY + x, col);
    draw_line(cX - y, cY - x, cX + y, cY - x, col);
}

void Graphics::_draw_line_low(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t col) {
    int16_t dx = x1 - x0;
    int16_t dy = y1 - y0;
    int16_t  yi = 1;
    if (dy < 0) {
        yi = -1;
        dy = -dy;
    }
    
    int16_t D = (2 * dy) - dx;
    int16_t y = y0;

    if (x0 < x1){
        for (int16_t x = x0; x <= x1; x++){
            _set_pixel(x, y, col);
            if (D > 0){
                y = y + yi;
                D = D - 2*dx;
            }
            D = D + 2*dy;
        }
    }
    else{
        for (int16_t x = x0; x >= x1; x--){
            _set_pixel(x, y, col);
            if (D > 0){
                y = y + yi;
                D = D - 2*dx;
            }
            D = D + 2*dy;
        }
    }
}
void Graphics::_draw_line_high(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t col) {
    int16_t dx = x1 - x0;
    int16_t dy = y1 - y0;
    int16_t xi = 1;

    if (dx < 0) {
        xi = -1;
        dx = -dx;
    }
    
    int16_t D = (2 * dx) - dy;    
    int16_t x = x0;

    if (y0 < y1){
        for (int16_t y = y0; y <= y1; y++){
            _set_pixel(x, y, col);
            if (D > 0){
                x = x + xi;
                D = D - 2*dy;
            }
            D = D + 2*dx;
        }
    }
    else{
        for (int16_t y = y0; y >= y1; y--){
            _set_pixel(x, y, col);
            if (D > 0){
                x = x + xi;
                D = D - 2*dy;
            }
            D = D + 2*dx;
        }
    }
}