#pragma once 

#include "stdint.h"
#include "hardware/spi.h"

#define GPIOA 0
#define GPIOB 0
#define GPIOC 0
#define GPIOD 0
#define GPIOE 0

#define GPIO_PIN_0 13
#define GPIO_PIN_1 12
#define GPIO_PIN_2 11

extern spi_inst_t* SSD1350_SPI;

void SPI_TXBuffer(uint8_t *buffer, uint32_t len);
void SPI_TXByte(uint8_t data);
void GPIO_SetPin(uint16_t Port, uint16_t Pin);
void GPIO_ResetPin(uint16_t Port, uint16_t Pin);
void HAL_Delay(uint16_t ms);