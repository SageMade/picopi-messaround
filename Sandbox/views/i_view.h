#pragma once

#include <atomic>
#include "enums.h"
#include "custom-driver/graphics.h"

/*
 * Base class for views that render some content
*/
class IView {
    public:
        virtual ~IView() = default;

        virtual void update() = 0;
        virtual void render(int selectorDelta, ButtonState selectorButton, Graphics& display) = 0;
        virtual void show() = 0;
        virtual void hide() = 0;

        virtual const char* get_name() const = 0;

        inline static std::atomic<int> Selector = 0;

    protected:
        IView() = default;
};