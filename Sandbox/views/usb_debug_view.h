#pragma once
#include <vector>
#include <string>
#include "i_view.h"
#include "custom-driver/graphics.h"
#include "enums.h"

class USBDebugView : public IView {
    public:
        USBDebugView();
        virtual ~USBDebugView();

        virtual void update() override;
        virtual void render(int selectorDelta, ButtonState selectorButton, Graphics& display) override;
        virtual void show() override;
        virtual void hide() override;

        virtual const char* get_name() const override;

    protected:
        char testText_[128];
};