#pragma once
#include <vector>
#include "i_view.h"
#include "custom-driver/graphics.h"
#include "enums.h"

class JoystickView : public IView {
    public:
        struct Joystick {
            const uint8_t adc_channelX;
            const uint8_t adc_channelY;
            const uint8_t switch_pin;

            float x, y;
            ButtonState state;

            uint16_t dotColor;

            Joystick(uint8_t switch_pin, uint8_t adc_channelX, uint8_t adc_channelY) :
                adc_channelX(adc_channelX),
                adc_channelY(adc_channelY),
                switch_pin(switch_pin),
                x(0.0f), y(0.0f), state(ButtonState::Released),
                dotColor(COLOR_RED)
            { }
        };

        JoystickView();
        virtual ~JoystickView();

        void add_joystick(const Joystick& stick);
        void add_joystick(uint8_t switch_pin, uint8_t acd_channelX, uint8_t acd_channelY, uint16_t color = COLOR_RED);
    
        virtual void update() override;
        virtual void render(int selectorDelta, ButtonState selectorButton, Graphics& display) override;
        virtual void show() override;
        virtual void hide() override;

        virtual const char* get_name() const override;

    protected:
        std::vector<Joystick> joysticks_;
};