#include "usb_debug_view.h"

#include "pico/stdlib.h"
#include "hardware/adc.h"

#include "hardware.h"
#include "usb_keyboard.h"

USBDebugView::USBDebugView() = default;

USBDebugView::~USBDebugView() = default;

void USBDebugView::update() {
    
}

void USBDebugView::render(int selectorDelta, ButtonState selectorButton, Graphics& display) {
    const vec<2, int16_t>& cursor = display.get_cursor();
    const int16_t& x = cursor.x;
    const int16_t& y = cursor.y;
    //SSD1351_get_cursor(&x, &y);

    switch (usb_state)
    {
    case USBState::DISCONNECTED:
        display.print_string(&Font_7x10, COLOR_RED, "Status: UNMOUNTED\n");
        break;
    case USBState::MOUNTED:
        display.print_string(&Font_7x10, COLOR_GREEN, "Status: MOUNTED\n");
        break;
    case USBState::SUSPENDED:
        display.print_string(&Font_7x10, COLOR_YELLOW, "Status: SUSPENDED\n");
        break;
    
    default:
        break;
    }

    display.printf(&Font_7x10, COLOR_YELLOW, "Mem: %s\n", testText_);

    for(int ix = 0; ix < config.known_dev_count; ix++) {
        display.printf(&Font_7x10, COLOR_YELLOW, "Dev %d: 0x%x\n", ix, config.known_i2c_devs[ix]);
    }
}

void USBDebugView::show() {
    memory.read(0x0000, testText_, 128);
    HwPinout temp;
    memory.read(0x7FFF - sizeof(HwPinout), temp);
    testText_[127] = temp.known_dev_count;
}

void USBDebugView::hide() {

}

const char* USBDebugView::get_name() const {
    return "USB Debug";
}