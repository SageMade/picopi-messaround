#pragma once

#include "i_view.h"
#include "custom-driver/graphics.h"

class HardwareInspectView : public IView {
    public:
        HardwareInspectView();
        virtual ~HardwareInspectView();
    
        virtual void update() override;
        virtual void render(int selectorDelta, ButtonState selectorButton, Graphics& display) override;
        virtual void show() override;
        virtual void hide() override;
        
        virtual const char* get_name() const override;
};