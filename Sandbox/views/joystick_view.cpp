#include "joystick_view.h"

#include "pico/stdlib.h"
#include "hardware/adc.h"

#include "hardware.h"

JoystickView::JoystickView() = default;

JoystickView::~JoystickView() = default;

void JoystickView::add_joystick(const JoystickView::Joystick& stick) {
    joysticks_.push_back(stick);

    gpio_init(stick.switch_pin);
    gpio_set_dir(stick.switch_pin, GPIO_IN);
    gpio_pull_up(stick.switch_pin);
}

void JoystickView::add_joystick(uint8_t switch_pin,  uint8_t acd_channelX, uint8_t acd_channelY, uint16_t color) {
    Joystick result = Joystick(switch_pin, acd_channelX, acd_channelY);
    result.dotColor = color;

    add_joystick(result);
}

void JoystickView::update() {
    for(auto& stick : joysticks_) {
        uint16_t adc0 = state.adc[stick.adc_channelX];
        uint16_t adc1 = state.adc[stick.adc_channelY];
        
        stick.x = ((adc0 / 4096.0f) * 2.0f) - 1.0f;
        stick.y = ((adc1 / 4096.0f) * 2.0f) - 1.0f;

        uint8_t cState = (uint8_t)( stick.state);
        cState << 1;
        cState &= 0b10;
        cState = cState | ((!gpio_get(stick.switch_pin) ? 1 : 0) & 0b01);
        stick.state = (ButtonState)cState;
    }
}

void JoystickView::render(int selectorDelta, ButtonState selectorButton, Graphics& display) {
    const vec<2, int16_t>& cursor = display.get_cursor();
    const int16_t& x = cursor.x;
    const int16_t& y = cursor.y;
    //SSD1351_get_cursor(&x, &y);

    display.draw_rect(x, y, 64, 64, COLOR_WHITE);
    display.draw_line(x + 32, y, x + 32, y + 64, COLOR_WHITE);
    display.draw_line(x, y + 32, x + 64, y + 32, COLOR_WHITE);

    int radius = joysticks_.size() + 1;

    for(auto& stick : joysticks_) {
        uint16_t xPos = (x + 32) + (stick.x * 32);
        uint16_t yPos = (y + 32) + (stick.y * 32);

        if (IsDown(stick.state)) {
            display.fill_circle(xPos, yPos, radius, stick.dotColor);
        } 
        else  {
            display.draw_circle(xPos, yPos, radius, stick.dotColor);
        }
    }
}

void JoystickView::show() {

}
void JoystickView::hide() {

}

const char* JoystickView::get_name() const {
    return "Joystick View";
}