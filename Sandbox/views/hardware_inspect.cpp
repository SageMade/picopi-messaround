#include "hardware_inspect.h"

#include "pico/stdlib.h"
#include "hardware/adc.h"
#include "hardware.h"

HardwareInspectView::HardwareInspectView()
{ }

HardwareInspectView::~HardwareInspectView() = default;

void HardwareInspectView::update() { }

void HardwareInspectView::render(int selectorDelta, ButtonState selectorButton, Graphics& display) {
    const vec<2, int16_t>& cursor = display.get_cursor();
    int16_t y = cursor.y;
    //SSD1351_get_cursor(&x, &y);

    for( uint16_t ix = 0; ix < 8; ix++) {
        display.printf(&Font_7x10, COLOR_WHITE, "%0i: %u\n", ix, state.adc[ix]);
    }
    for( uint16_t ix = 8; ix < 16; ix++) {
        display.set_cursor(60, y);
        display.printf(&Font_7x10, COLOR_WHITE, "%0i: %u\n", ix, state.adc[ix]);
        y = cursor.y;
    }
}

void HardwareInspectView::show() {

}
void HardwareInspectView::hide() {

}

const char* HardwareInspectView::get_name() const {
    return "ADC Inspector";
}