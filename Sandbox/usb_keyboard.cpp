#include "usb_keyboard.h"

#include "bsp/board.h"
#include "tusb.h"
#include "usb_descriptors.h"
#include "pico/stdlib.h"

#include <cstring>
#include <cstdlib>

USBState usb_state = USBState::DISCONNECTED;

void tusb_send_hid_report(int report_id) 
{
      // skip if hid is not ready yet
  if ( !tud_hid_ready() ) return;

  switch(report_id)
  {
    case REPORT_ID_KEYBOARD:
    {
      // use to avoid send multiple consecutive zero report for keyboard
      static bool has_keyboard_key = false;

      if ( !gpio_get(2) )
      {
        uint8_t keycode[6] = { 0 };
        keycode[0] = HID_KEY_A;

        tud_hid_keyboard_report(REPORT_ID_KEYBOARD, 0, keycode);
        has_keyboard_key = true;
      }else
      {
        // send empty key report if previously has key pressed
        if (has_keyboard_key) tud_hid_keyboard_report(REPORT_ID_KEYBOARD, 0, NULL);
        has_keyboard_key = false;
      }
    }
    break;

    case REPORT_ID_MOUSE:
    {
      int8_t const delta = 5;

      // no button, right + down, no scroll, no pan
      tud_hid_mouse_report(REPORT_ID_MOUSE, 0x00, delta, delta, 0, 0);
    }
    break;

  /*
    case REPORT_ID_CONSUMER_CONTROL:
    {
      // use to avoid send multiple consecutive zero report
      static bool has_consumer_key = false;

      if ( btn )
      {
        // volume down
        uint16_t volume_down = HID_USAGE_CONSUMER_VOLUME_DECREMENT;
        tud_hid_report(REPORT_ID_CONSUMER_CONTROL, &volume_down, 2);
        has_consumer_key = true;
      }else
      {
        // send empty key report (release key) if previously has key pressed
        uint16_t empty_key = 0;
        if (has_consumer_key) tud_hid_report(REPORT_ID_CONSUMER_CONTROL, &empty_key, 2);
        has_consumer_key = false;
      }
    }
    break;

    case REPORT_ID_GAMEPAD:
    {
      // use to avoid send multiple consecutive zero report for keyboard
      static bool has_gamepad_key = false;

      hid_gamepad_report_t report =
      {
        .x   = 0, .y = 0, .z = 0, .rz = 0, .rx = 0, .ry = 0,
        .hat = 0, .buttons = 0
      };

      if ( btn )
      {
        report.hat = GAMEPAD_HAT_UP;
        report.buttons = GAMEPAD_BUTTON_A;
        tud_hid_report(REPORT_ID_GAMEPAD, &report, sizeof(report));

        has_gamepad_key = true;
      }else
      {
        report.hat = GAMEPAD_HAT_CENTERED;
        report.buttons = 0;
        if (has_gamepad_key) tud_hid_report(REPORT_ID_GAMEPAD, &report, sizeof(report));
        has_gamepad_key = false;
      }
    }
    break;
  */

    default: break;
  }
}

void tusb_hid_task() {
    // Poll every 10ms
    const uint32_t interval_ms = 10;
    static uint32_t start_ms = 0;

    if ( board_millis() - start_ms < interval_ms) return; // not enough time
    start_ms += interval_ms;

    // Remote wakeup
    if ( tud_suspended() )
    {
        // Wake up host if we are in suspend mode
        // and REMOTE_WAKEUP feature is enabled by host
        tud_remote_wakeup();
    }
}
// Invoked when sent REPORT successfully to host
// Application can use this to send the next report
// Note: For composite reports, report[0] is report ID
void tud_hid_report_complete_cb(uint8_t instance, uint8_t const* report, uint8_t len)
{
  (void) instance;
  (void) len;

  printf("Message received");
}

// Invoked when received GET_REPORT control request
// Application must fill buffer report's content and return its length.
// Return zero will cause the stack to STALL request
uint16_t tud_hid_get_report_cb(uint8_t instance, uint8_t report_id, hid_report_type_t report_type, uint8_t* buffer, uint16_t reqlen)
{
  // TODO not Implemented
  (void) instance;
  (void) report_id;
  (void) report_type;
  (void) buffer;
  (void) reqlen;
  
  hid_keyboard_report_t report;

  bool const btn = !gpio_get(2);

  if ( btn )
  {
    uint8_t keycode[6] = { 0 };
    keycode[0] = HID_KEY_CAPS_LOCK;

    memcpy(report.keycode, keycode, 6);
  }

  memcpy(buffer, (uint8_t*)&report, 8);

  return 8;

  return 0;
}

// Invoked when received SET_REPORT control request or
// received data on OUT endpoint ( Report ID = 0, Type = 0 )
void tud_hid_set_report_cb(uint8_t instance, uint8_t report_id, hid_report_type_t report_type, uint8_t const* buffer, uint16_t bufsize)
{
  (void) instance;

  if (report_type == HID_REPORT_TYPE_OUTPUT)
  {
    // Set keyboard LED e.g Capslock, Numlock etc...
    if (report_id == REPORT_ID_KEYBOARD)
    {
      // bufsize should be (at least) 1
      if ( bufsize < 1 ) return;

      uint8_t const kbd_leds = buffer[0];

      
      if (kbd_leds & KEYBOARD_LED_CAPSLOCK)
      {
        gpio_put(25, true);
      }else
      {
        gpio_put(25, false);
      }
    }
  }
}

void update_usb() {
    tud_task();
    tusb_hid_task();
}
void init_usb() {
    tusb_init();
}

Keyboard::Keyboard() :
  keys_ {0, 0, 0, 0, 0, 0},
  offset_(0),
  modifier_(0)
{ }

bool Keyboard::hold_key(uint8_t keycode) {
  if (offset_ >= 6) {
    return false;
  }

  if (keycode == HID_KEY_CONTROL_LEFT) {
    modifier_ |= 0b00000001;
  }
  else if (keycode == HID_KEY_SHIFT_LEFT) {
    modifier_ |= 0b00000010;
  }
  else if (keycode == HID_KEY_ALT_LEFT) {
    modifier_ |= 0b00000100;
  }
  else if (keycode == HID_KEY_GUI_LEFT) {
    modifier_ |= 0b00001000;
  }
  else if (keycode == HID_KEY_CONTROL_RIGHT) {
    modifier_ |= 0b00010000;
  }
  else if (keycode == HID_KEY_SHIFT_RIGHT) {
    modifier_ |= 0b00100000;
  }
  else if (keycode == HID_KEY_ALT_RIGHT) {
    modifier_ |= 0b01000000;
  }
  else if (keycode == HID_KEY_GUI_RIGHT) {
    modifier_ |= 0b10000000;
  }
  else {
    keys_[offset_++] = keycode;
    tud_hid_keyboard_report(REPORT_ID_KEYBOARD, modifier_, keys_);
    sleep_ms(15);
  }
  return true;
}

bool Keyboard::press_key(uint8_t keycode) {
  uint8_t keys[6] = { 0 };
  keys[0] = keycode;

  tud_hid_keyboard_report(REPORT_ID_KEYBOARD, modifier_, keys);
  sleep_ms(20);
  tud_hid_keyboard_report(REPORT_ID_KEYBOARD, modifier_, NULL);
  sleep_ms(20);

  return true;
}

bool Keyboard::release_key(uint8_t keycode) {
  if (keycode == HID_KEY_CONTROL_LEFT) {
    modifier_ &= ~0b00000001;
  }
  else if (keycode == HID_KEY_SHIFT_LEFT) {
    modifier_ &= ~0b00000010;
  }
  else if (keycode == HID_KEY_ALT_LEFT) {
    modifier_ &= ~0b00000100;
  }
  else if (keycode == HID_KEY_GUI_LEFT) {
    modifier_ &= ~0b00001000;
  }
  else if (keycode == HID_KEY_CONTROL_RIGHT) {
    modifier_ &= ~0b00010000;
  }
  else if (keycode == HID_KEY_SHIFT_RIGHT) {
    modifier_ &= ~0b00100000;
  }
  else if (keycode == HID_KEY_ALT_RIGHT) {
    modifier_ &= ~0b01000000;
  }
  else if (keycode == HID_KEY_GUI_RIGHT) {
    modifier_ &= ~0b10000000;
  }

  return true;
}
bool Keyboard::press_sequence(std::vector<uint8_t> keys) {
  return press_sequence(keys.data(), keys.size());
}
bool Keyboard::press_sequence(uint8_t* keys, uint32_t size) {
  bool result = true;

  release_all();
  
  for (uint32_t ix = 0; ix < size; ix++) {
    uint8_t key = *(keys + ix);
    if (key == HID_KEY_CONTROL_LEFT) {
      modifier_ ^= 0b00000001;
    }
    else if (key == HID_KEY_SHIFT_LEFT) {
      modifier_ ^= 0b00000010;
    }
    else if (key == HID_KEY_ALT_LEFT) {
      modifier_ ^= 0b00000100;
    }
    else if (key == HID_KEY_GUI_LEFT) {
      modifier_ ^= 0b00001000;
    }
    else if (key == HID_KEY_CONTROL_RIGHT) {
      modifier_ ^= 0b00010000;
    }
    else if (key == HID_KEY_SHIFT_RIGHT) {
      modifier_ ^= 0b00100000;
    }
    else if (key == HID_KEY_ALT_RIGHT) {
      modifier_ ^= 0b01000000;
    }
    else if (key == HID_KEY_GUI_RIGHT) {
      modifier_ ^= 0b10000000;
    }
    else if (key == HID_KEY_NONE) {
      uint32_t mult = keys[++ix];
      uint32_t dur = keys[++ix];
      sleep_ms(mult * dur);
    }
    else if (key == 0xFF) {
      release_all();
    }
    else {
      press_key(key);
    }
  }

  release_all();

  return true;
}

void Keyboard::flush(uint8_t& offset, uint8_t modifier, uint8_t toPress[6]) {  
    if (offset > 0) {
      tud_hid_keyboard_report(REPORT_ID_KEYBOARD, modifier, toPress);
      sleep_ms(10);
      memset(toPress, 0, 6);
      offset = 0;
    }
}

char keyMap[128] = {
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // 0 - 7
  0xFF, 0x2B, 0x28, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // 8 - 15
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // 16 - 23
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // 24 - 31
  0x2C, 0x1E, 0x34, 0x20, 0x21, 0x21, 0x24, 0x34, // 32 - 39
  0x26, 0x27, 0x25, 0x2E, 0x36, 0x2D, 0x37, 0x38, // 40 - 47
  0x27, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23, 0x24, // 48 - 55
  0x25, 0x26, 0x33, 0x33, 0x36, 0x2E, 0x37, 0x38, // 56 - 63
  0x1F, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, // 64 - 71
  0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, // 72 - 79
  0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, // 80 - 87
  0x1B, 0x1C, 0x1D, 0x2F, 0x31, 0x30, 0x23, 0x2D, // 88 - 95
  0x35, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, // 96 - 103
  0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, // 104 - 111
  0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, // 112 - 119
  0x1B, 0x1C, 0x1D, 0x2F, 0x31, 0x30, 0x35, 0xFF
};
bool shiftMap[128] = {
  0, 0, 0, 0, 0, 0, 0, 0, // 0 - 7
  0, 0, 0, 0, 0, 0, 0, 0, // 8 - 15
  0, 0, 0, 0, 0, 0, 0, 0, // 16 - 23
  0, 0, 0, 0, 0, 0, 0, 0, // 24 - 31
  0, 1, 1, 1, 1, 1, 1, 0, // 32 - 39
  1, 1, 1, 1, 0, 0, 0, 0, // 40 - 47
  0, 0, 0, 0, 0, 0, 0, 0, // 48 - 55
  0, 0, 1, 0, 1, 0, 1, 1, // 56 - 63
  1, 1, 1, 1, 1, 1, 1, 1, // 64 - 71
  1, 1, 1, 1, 1, 1, 1, 1, // 72 - 79
  1, 1, 1, 1, 1, 1, 1, 1, // 80 - 87
  1, 1, 1, 0, 0, 0, 1, 1, // 88 - 95
  0, 0, 0, 0, 0, 0, 0, 0, // 96 - 103
  0, 0, 0, 0, 0, 0, 0, 0, // 104 - 111
  0, 0, 0, 0, 0, 0, 0, 0, // 112 - 119
  0, 0, 0, 1, 1, 1, 1, 0
};

Keyboard::KeySequence Keyboard::gen_sequence(const char* text) {
  KeySequence result;
  
  size_t len = strlen(text);
  size_t seqSize = 0;

  bool upper = 0;
  for(int ix = 0; ix < len; ix++) {
    char c = text[ix];
    if (c > 128) {
      continue;
    }
    if (shiftMap[c] != upper) {
      seqSize++;
    }
    if (keyMap[c] != 0xFF) {
      seqSize++;
    }
  }
  size_t offset = 0;

  result.data = (uint8_t*)malloc(seqSize);
  result.size = seqSize;
  
  upper = 0;
  for(int ix = 0; ix < len; ix++) {
    char c = text[ix];
    if (c > 128) {
      continue;
    }
    if (shiftMap[c] != upper) {
      result.data[offset++] = HID_KEY_SHIFT_LEFT;
      upper = shiftMap[c];
    }
    if (keyMap[c] != 0xFF) {
      result.data[offset++] = keyMap[c];
    }
  }
  
  return result;
}

void Keyboard::free_sequence(Keyboard::KeySequence& seq) {
  free(seq.data);
  seq.data = nullptr;
  seq.size = 0;
}

void Keyboard::release_all() {
  memset(keys_, 0, 6);
  offset_ = 0;
  modifier_ = 0;
  tud_hid_keyboard_report(REPORT_ID_KEYBOARD, 0, NULL);
  sleep_ms(20);
}