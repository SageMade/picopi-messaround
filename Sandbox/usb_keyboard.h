#pragma once

#include <initializer_list>
#include <vector>
#include <stdint.h>

enum USBState {
    DISCONNECTED = 0,
    MOUNTED   = 1,
    SUSPENDED = 2
};

extern USBState usb_state;

void update_usb();
void init_usb();

class Keyboard {
    public:
        Keyboard();

        struct KeySequence {
            uint8_t* data;
            uint32_t size;
        };

        bool hold_key(uint8_t keycode);
        bool press_key(uint8_t keycode);
        bool press_sequence(std::vector<uint8_t> keys);
        bool press_sequence(uint8_t* keys, uint32_t size);
        bool release_key(uint8_t keycode);
        void release_all();

        static KeySequence gen_sequence(const char* text);
        static void free_sequence(KeySequence& seq);
    private:
        uint8_t keys_[8];
        uint8_t modifier_;
        uint8_t offset_;

        static void flush(uint8_t& offset, uint8_t modifier, uint8_t toPress[6]);
};