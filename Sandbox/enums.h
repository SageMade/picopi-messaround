#pragma once

enum class ButtonState : uint8_t {
    Up       = 0b00,
    Pressed  = 0b01,
    Down     = 0b11,
    Released = 0b10
};

inline bool IsDown(ButtonState state) {
    return ((uint8_t)state) & 0b01;
}