#include "HAL.h"
#include "pico/stdlib.h"

spi_inst_t* SSD1350_SPI = spi1;

void SPI_TXBuffer(uint8_t *buffer, uint32_t len) {
    spi_write_blocking(SSD1350_SPI, buffer, len);
}
void SPI_TXByte(uint8_t data) {
    spi_write_blocking(SSD1350_SPI, &data, 1);
}
void GPIO_SetPin(uint16_t Port, uint16_t Pin) {
    gpio_put(Pin, true);
}
void GPIO_ResetPin(uint16_t Port, uint16_t Pin) {
    gpio_put(Pin, false);
}
void HAL_Delay(uint16_t ms) {
    sleep_ms(ms);
}