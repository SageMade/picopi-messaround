#pragma once

#include <cstdint>
#include "enums.h"
#include <pico/time.h>

class RotaryEncoder {
    public:
        RotaryEncoder(int pin_clk, int pin_dt, int pin_button = -1);
        ~RotaryEncoder();

        void init();

        int process();
        void poll();
        ButtonState get_button() const;

        absolute_time_t lastReleasedTime;

    private:
        int clk_pin;
        int dt_pin;
        int btn_pin;

        uint8_t state;
        ButtonState buttonState;
};