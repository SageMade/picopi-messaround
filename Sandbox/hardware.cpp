#include "hardware.h"

#include "pico/stdlib.h"
#include "hardware/adc.h"
#include "hardware/i2c.h"

HwState state = HwState();
HwPinout config = HwPinout();
EEPROM memory = EEPROM(i2c1);

uint16_t read_external_adc(uint8_t index, const uint8_t ctrlPins[4], uint8_t inAdc) {
    gpio_put(ctrlPins[0], (0x01 << 0) & index);
    gpio_put(ctrlPins[1], (0x01 << 1) & index);
    gpio_put(ctrlPins[2], (0x01 << 2) & index);
    gpio_put(ctrlPins[3], (0x01 << 3) & index);
    adc_select_input(inAdc);
    sleep_us(10);    
    return adc_read();
}

void update_hardware_state() {
    for(uint16_t ix = 0; ix < 16; ix++) {
        state.adc[ix] = read_external_adc(ix, config.adc_ctrl_pins, 0);
    }

    for (size_t ix = 0; ix < HW_IN_PIN_COUNT; ix++) {
        state.in_pin_state[ix] = gpio_get(config.in_pins[ix].pin);
    }
}

void init_hardware() {
    // Open all on-chip ADC ports
    adc_init();
    adc_gpio_init(26);
    adc_gpio_init(27);
    adc_gpio_init(28);

    for(size_t ix = 0; ix < 4; ix ++) {
        gpio_init(config.adc_ctrl_pins[ix]);
        gpio_set_dir(config.adc_ctrl_pins[ix], GPIO_OUT);
    }
    
    for(size_t ix = 0; ix < HW_OUT_PIN_COUNT; ix ++) {
        gpio_init(config.out_pins[ix]);
        gpio_set_dir(config.out_pins[ix], GPIO_OUT);
    }
    
    for(size_t ix = 0; ix < HW_IN_PIN_COUNT; ix ++) {
        gpio_init(config.out_pins[ix]);
        gpio_set_dir(config.out_pins[ix], GPIO_IN);
        if (config.in_pins[ix].mode == HW_PIN_PULL_HIGH) {            
            gpio_pull_up(config.in_pins[ix].pin);
        }
        else if (config.in_pins[ix].mode == HW_PIN_PULL_LOW) {
            gpio_pull_down(config.in_pins[ix].pin);
        }
    }
}
