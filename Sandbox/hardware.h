#pragma once

#include <cstdint>

#define HW_IN_PIN_COUNT 3
#define HW_OUT_PIN_COUNT 4 

#define HW_PIN_PULL_LOW 1
#define HW_PIN_PULL_HIGH 2

#include "custom-driver/eeprom.h"

struct HwState {
    uint16_t adc[16];  

    bool in_pin_state[HW_IN_PIN_COUNT];
};

struct InPin {
    uint8_t pin;
    uint8_t mode;
};

struct HwPinout {
    const uint8_t  adc_ctrl_pins[4] { 19, 18, 17, 16 };

    const uint16_t out_pins[HW_OUT_PIN_COUNT] { 25, 11, 12, 13 };
    const InPin    in_pins[HW_IN_PIN_COUNT] {
        { 21, HW_PIN_PULL_HIGH }, 
        { 8, HW_PIN_PULL_LOW }, 
        { 9, HW_PIN_PULL_LOW }
    };

    uint8_t known_i2c_devs[128] = { 0 };
    uint8_t known_dev_count;
};

void init_hardware();
void update_hardware_state();
uint16_t read_external_adc(uint8_t index, const uint8_t ctrlPins[4], uint8_t inAdc);

extern EEPROM memory;
extern HwState state;
extern HwPinout config;