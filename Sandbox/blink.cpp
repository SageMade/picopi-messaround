#include <stdio.h>
#include <cstring>
#include <stdio.h>

#include "pico/stdlib.h"
#include "pico/multicore.h"
#include "hardware/adc.h"
#include "hardware/i2c.h"
#include <pico/time.h>
#include <tusb.h>

#include "custom-driver/ssd1351.h"    

#include "hardware.h"
#include "RotaryEncoder.h"
#include "views/i_view.h"
#include "views/joystick_view.h"
#include "views/hardware_inspect.h"
#include "views/usb_debug_view.h"

#include "custom-driver/eeprom.h"

#include "usb_keyboard.h"

const uint LED_PIN = 5;
uint16_t adc0 = 0;
uint16_t adc1 = 0;
uint16_t adc2 = 0;

RotaryEncoder menu = RotaryEncoder(4, 3, 2);
int mode = 0;
bool drawMenu = false;
int16_t scroll = 0;
uint16_t selection = 0;

#define NUM_VIEWS 8

Ssd1351* display = nullptr;
IView* currentView = nullptr;
IView* views[NUM_VIEWS];

int viewIx = 0;

bool keyboardMode = false;

// Invoked when device is mounted
void tud_mount_cb(void)
{
    usb_state = USBState::MOUNTED;
}

// Invoked when device is unmounted
void tud_umount_cb(void)
{
    usb_state = USBState::DISCONNECTED;
}

// Invoked when usb bus is suspended
// remote_wakeup_en : if host allow us  to perform remote wakeup
// Within 7ms, device must draw an average of current less than 2.5 mA from bus
void tud_suspend_cb(bool remote_wakeup_en)
{
  (void) remote_wakeup_en;
    usb_state = USBState::SUSPENDED;
}

// Invoked when usb bus is resumed
void tud_resume_cb(void)
{
    usb_state = USBState::MOUNTED;
}

void gpio_callback(uint pin, uint32_t event) {
    mode -= menu.process();
}

void update_USB() {    
    while (true) {
        if (keyboardMode) {
            update_usb();
        }
        sleep_ms(1);
    }
}

bool reserved_addr(uint8_t addr) {
    return (addr & 0x78) == 0 || (addr & 0x78) == 0x78;
}


int main() {    
    init_hardware();
    gpio_init(LED_PIN);    
    gpio_set_dir(LED_PIN, GPIO_OUT);
    gpio_put(LED_PIN, true);
    sleep_ms(250);
    gpio_put(LED_PIN, false);
    sleep_ms(500);
    
    keyboardMode = gpio_get(8);
    if (keyboardMode) {
        init_usb();
    }
    else {
        // Init std io stuff
        //stdio_init_all();
        //stdio_usb_init();
        
        gpio_put(LED_PIN, true);
        sleep_ms(250);
            
        const int I2C_PIN_CLK = 7;
        const int I2C_PIN_SDA = 6;

        i2c_inst_t* i2c = i2c1;

        gpio_set_function(I2C_PIN_CLK, GPIO_FUNC_I2C);
        gpio_set_function(I2C_PIN_SDA, GPIO_FUNC_I2C);

        gpio_pull_up(I2C_PIN_CLK);
        gpio_pull_up(I2C_PIN_SDA);
        
        i2c_init(i2c, 100 * 1000);
                
        //bi_decl(bi_2pins_with_func(I2C_PIN_SDA, I2C_PIN_CLK, GPIO_FUNC_I2C));
                
        //printf("\nI2C Bus Scan\n");
        //printf("   0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F\n");
    
        config.known_dev_count = 0;
        
        for (int addr = 0; addr < (1 << 7); ++addr) {
            if (addr % 16 == 0) {
               //printf("%02x ", addr);
            }
    
            // Perform a 1-byte dummy read from the probe address. If a slave
            // acknowledges this address, the function returns the number of bytes
            // transferred. If the address byte is ignored, the function returns
            // -1.
    
            // Skip over any reserved addresses.
            int ret;
            uint8_t rxdata;
            if (reserved_addr(addr))
                ret = PICO_ERROR_GENERIC;
            else 
                ret = i2c_read_blocking(i2c, addr, &rxdata, 1, false);
            
            if (ret >= 0) {
                config.known_i2c_devs[config.known_dev_count++] = addr;
            }
    
            //printf(ret < 0 ? "." : "@");
            //printf(addr % 16 == 15 ? "\n" : "  ");
        }

        uint16_t address = 0x0F0F;
        uint8_t value = 0xFE;
        //uint8_t commandBuffer[] = {
        //    (uint8_t)(address >> 8),
        //    (uint8_t)(address & 0xFF),
        //    value
        //};
        
        //uint8_t commandBuffer[] = {
        //    (uint8_t)(address >> 8),
        //    (uint8_t)(address & 0xFF)
        //};

        //uint8_t receiveBuffer[64];

        //i2c_write_blocking(i2c, 0x50, commandBuffer, 3, true);
        //i2c_read_blocking(i2c, 0x50, receiveBuffer, 1, true);

        uint16_t cfgAddress = 0x7FFF - sizeof(HwPinout); // max address
        memory.write(cfgAddress, config);

        const char* test = "Hello world!";
        memory.write(0x0000, test, strlen(test) + 1);

        //if (receiveBuffer[0] == value) {
            gpio_put(LED_PIN, true);
        //}
        //printf("Done.\n");
    }
    
    printf("Commencing startup sequence\n");

    spi_init(spi1, 18000000);    
    
    spi_set_format(spi1,   // SPI instance
                    8,      // Number of bits per transfer
                    SPI_CPOL_1,      // Polarity (CPOL)
                    SPI_CPHA_1,      // Phase (CPHA)
                    SPI_MSB_FIRST);

    gpio_set_function(14, GPIO_FUNC_SPI);
    gpio_set_function(15, GPIO_FUNC_SPI);

    //gpio_put(LED_PIN, true);
    sleep_ms(500);
    
    menu.init();
    gpio_set_irq_enabled_with_callback(3, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true, &gpio_callback);
    gpio_set_irq_enabled(4, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true);

    JoystickView* joysticks = new JoystickView();
    joysticks->add_joystick(22, 0, 1, COLOR_RED);
    joysticks->add_joystick(20, 2, 3, COLOR_GREEN);

    views[0] = joysticks;
    views[1] = new HardwareInspectView();
    views[2] = new USBDebugView();
    views[3] = joysticks;
    views[4] = joysticks;
    views[5] = joysticks;
    views[6] = joysticks;
    views[7] = joysticks;

    printf("Passed hardware init, moving on to OLED config...\n");

    Ssd1351Config cfg = Ssd1351Config();
    cfg.cs_pin  = 11;
    cfg.dc_pin  = 12;
    cfg.rst_pin = 13;
    cfg.width   = 128;
    cfg.height  = 128;
    cfg.sharedRAM = nullptr;
    //
    display = new Ssd1351(spi1, cfg);
    Ssd1351& disp = *display;
    Graphics& g = disp.getGraphics();
    
    gpio_put(LED_PIN, false);
    sleep_ms(500);
    gpio_put(LED_PIN, true);
    
    printf("OLED init finished, testing API...\n");

    disp.getGraphics().clear(COLOR_BLACK);
    g.set_cursor(0, 1);
    if (keyboardMode) {
        g.print_string(&Font_7x10, COLOR_WHITE, "Initializing USB interface...");
    }
    disp.update();
    
    if (keyboardMode) {
        multicore_launch_core1(update_USB);
    }

    if (keyboardMode) {
        g.print_string(&Font_7x10, COLOR_WHITE, "Core launched");
    }
    disp.update();

    printf("Test complete, moving to loop\n");

    uint16_t valuesX[64];
    uint16_t valuesY[64];
    memset(valuesX, 0, sizeof(valuesX));
    memset(valuesY, 0, sizeof(valuesY));
    uint8_t index = 0;

    Keyboard keyboard;

    Keyboard::KeySequence text = Keyboard::gen_sequence("Did you ever hear the tragedy of Darth Plagueis The Wise?\nI thought not. It's not a story the Jedi would tell you.\nIt's a Sith legend.\nDarth Plagueis was a Dark Lord of the Sith, so powerful and so wise he could use the Force to influence the midichlorians to create life...\nHe had such a knowledge of the dark side that he could even keep the ones he cared about from dying.\nThe dark side of the Force is a pathway to many abilities some consider to be unnatural.\nHe became so powerfull... the only thing he was afraid of was losing his power, which eventually, of course, he did.\nUnfortunately, he taught his apprentice everything he knew, then his apprentice killed him in his sleep.\nIronic.\nHe could save others from death, but not himself");
    while (true) {
        
        update_hardware_state();
        menu.poll();

        if (keyboardMode) {
            if (menu.get_button() == ButtonState::Pressed) {            
                /*keyboard.press_sequence({ 
                    HID_KEY_GUI_LEFT, HID_KEY_R, 0xFF,
                    HID_KEY_NONE, 1, 100,
                    HID_KEY_DELETE, 0xFF,
                    HID_KEY_NONE, 1, 100,
                    HID_KEY_N, HID_KEY_O, HID_KEY_T, HID_KEY_E, HID_KEY_P, HID_KEY_A, HID_KEY_D, 0xFF, 
                    HID_KEY_ENTER, 0xFF,
                    HID_KEY_NONE, 1, 100,
                    //HID_KEY_SHIFT_LEFT, HID_KEY_T, HID_KEY_SHIFT_LEFT, HID_KEY_H, HID_KEY_E, HID_KEY_SPACE, HID_KEY_Q, HID_KEY_U, HID_KEY_I, HID_KEY_C, HID_KEY_K, 
                    //HID_KEY_SPACE, HID_KEY_B, HID_KEY_R, HID_KEY_O, HID_KEY_W, HID_KEY_N, HID_KEY_SPACE, HID_KEY_F, HID_KEY_O, HID_KEY_X, 0xFF, 
                    HID_KEY_NONE, 1, 100,
                });
                */    
                keyboard.press_sequence(text.data, text.size);        
                sleep_ms(1000);
                keyboard.hold_key(HID_KEY_CONTROL_LEFT);
                keyboard.hold_key(HID_KEY_SHIFT_LEFT);
                keyboard.press_key(HID_KEY_ESCAPE);
                keyboard.release_all();
            }
        }
        
        absolute_time_t time = get_absolute_time();

        g.clear(COLOR_BLACK);
        g.set_cursor(0, 1);
        g.printf(&Font_7x10, COLOR_YELLOW, "Time: %u", to_ms_since_boot(time));
        
        if (IsDown(menu.get_button())) {
            g.fill_rect(128 - 8, 0, 8, 8, COLOR_YELLOW);
        }
        g.draw_rect(128 - 8, 0, 8, 8, COLOR_WHITE);

        if (gpio_get(20)) {
            g.fill_rect(128 - 16, 0, 8, 8, COLOR_YELLOW);
        }
        g.draw_rect(128 - 16, 0, 8, 8, COLOR_WHITE);
        g.draw_line(0, 8, 128, 8, COLOR_WHITE);
        
        g.set_cursor(0, 9);

        IView* nextView = views[viewIx];

        if (nextView != currentView) {
            if (currentView != nullptr) {
                currentView->hide();
            }
            nextView->show();
            currentView = nextView;
        }

        currentView->update();
        currentView->render(mode, menu.get_button(), display->getGraphics());

        static absolute_time_t timeMenuOpened = absolute_time_t();
        static bool skip = false;
        if (drawMenu) {            
            if (mode != 0) {
                if (mode > 0) {
                    viewIx = NUM_VIEWS + viewIx + 1;  
                }
                if (mode < 0) {
                    viewIx = NUM_VIEWS + viewIx - 1;  
                }
                viewIx %= NUM_VIEWS;
                mode = 0;
            }

            g.set_cursor(0, 9);
            uint16_t y = 0;

            if (viewIx > 3) {
                scroll = -(viewIx - 2) * 20;
            }
            else {
                scroll = 0;
            }

            g.set_scissor_rect(0, 0, 128, 81);
            for(int ix = 0; ix < NUM_VIEWS; ix++) {
                if (ix == viewIx) {
                    g.set_cursor(0, 9 + y + scroll);
                    g.fill_rect(0, y + scroll, 128, 20, COLOR_YELLOW);
                    g.draw_rect(0, y + scroll, 128, 20, COLOR_WHITE);
                    g.print_string(&Font_7x10, COLOR_BLACK, views[ix]->get_name());
                }
                else {
                    g.set_cursor(0, 9 + y + scroll);
                    g.fill_rect(0, y + scroll, 128, 20, COLOR_BLACK);
                    g.draw_rect(0, y + scroll, 128, 20, COLOR_WHITE);
                    g.print_string(&Font_7x10, COLOR_WHITE, views[ix]->get_name());
                }
                y += 20;
            }
            g.reset_scissor_rect();

            if (menu.get_button() == ButtonState::Released) {     
                if (skip) {
                    skip = false;
                }
                else {
                    drawMenu = false;
                    printf("Closing menu (%lld)\n", absolute_time_diff_us(menu.lastReleasedTime, time));
                }
            }
        }
        
        if (menu.get_button() == ButtonState::Pressed) {
            if (absolute_time_diff_us(menu.lastReleasedTime, time) < 250000) {
                drawMenu = true;
                scroll = 0;
                skip = true;
                printf("Opening menu (%lld)\n", absolute_time_diff_us(menu.lastReleasedTime, time));
            }
            else {
                printf("Not opening: %llu < %u\n",absolute_time_diff_us(menu.lastReleasedTime, time), 250000);
            }
        }

        display->update();
        sleep_ms(16);
        
        //bool down = !gpio_get(22);
        //gpio_put(LED_PIN, down);
        

        // We only have 3 analog inputs :|
        //adc_select_input(0);
        //adc0 = adc_read();
        //adc_select_input(1);
        //adc1 = adc_read();
        //adc_select_input(2);
        //adc2 = adc_read();
//
        //uint32_t gpio = gpio_get_all();
//
        //// Grab historical values for graph
        //valuesX[index] = adc1;
        //valuesY[index] = adc0;
        //index++;
        //if (index >= 64) {
        //    index = 0;
        //}
//
        //
        //mode = (mode + 3) % 3;
        //
        //SSD1351_fill(COLOR_BLACK);
        //SSD1351_set_cursor(0,0);
        //SSD1351_printf(COLOR_GREEN, small_font, "Mode: %u\n", mode, menu.get_button());
        //if (IsDown(menu.get_button())) {
        //    SSD1351_draw_filled_rect(128 - 8, 0, 8, 8, COLOR_GREEN);
        //}
        //SSD1351_draw_rect(128 - 8, 0, 8, 8, COLOR_WHITE);
//
        //SSD1351_draw_line(0, 9, 128, 9, COLOR_WHITE);
        //
        //uint8_t y = 0;
        //SSD1351_get_cursor(nullptr, &y);
        //SSD1351_draw_line(0, y, 128, y, COLOR_WHITE);
//
        //for(uint8_t ix = 1; ix < 64; ix++) {
        //    uint16_t value1 = valuesX[(ix + index - 1) % 64];
        //    uint16_t value2 = valuesX[(ix + index) % 64];
        //    float v1 = value1 / 4096.0f;
        //    float v2 = value2 / 4096.0f;
//
        //    SSD1351_draw_line(ix, y + 30 - (v1 * 30), ix + 1, y + 30 - (v2 * 30), COLOR_GREEN);
        //    
        //    value1 = valuesY[(ix + index - 1) % 64];
        //    value2 = valuesY[(ix + index) % 64];
        //    v1 = value1 / 4096.0f;
        //    v2 = value2 / 4096.0f;
//
        //    SSD1351_draw_line(ix + 64, y + 30 - (v1 * 30), ix + 65, y + 30 - (v2 * 30), COLOR_PURPLE);
        //}
        //SSD1351_draw_line(64, y, 64, y + 30, COLOR_WHITE);
        //SSD1351_set_cursor(0, 41);
        //y=41;
//
        //switch (mode)
        //{
        //    case 0: {
        //        SSD1351_draw_line(0, y, 128, y, COLOR_WHITE);
        //        SSD1351_draw_line(64, y, 64, y+64, COLOR_WHITE);
        //        SSD1351_draw_line(0, y+64, 64, y+64, COLOR_WHITE);
        //        SSD1351_draw_line(32, y, 32, y+64, COLOR_WHITE);
        //        SSD1351_draw_line(0, y+32, 64, y+32, COLOR_WHITE);
//
        //        uint16_t xPos = (adc1 / 4096.0f) * 64.0f;
        //        uint16_t yPos = (adc0 / 4096.0f) * 64.0f;
//
        //        SSD1351_draw_filled_circle(xPos, yPos + y, 4, COLOR_RED);
//
        //        bool down = !gpio_get(22);
        //        if (down) {
        //            SSD1351_draw_filled_rect(66, y + 1, 16, 16, COLOR_GREEN);
        //        }
        //        gpio_put(LED_PIN, down);
        //        SSD1351_draw_rect(66, y + 1, 16, 16, COLOR_WHITE);
        //        
        //        SSD1351_printf(COLOR_GREEN, small_font, "X: %u\n", adc1);
        //        SSD1351_printf(COLOR_PURPLE, small_font, "Y: %u\n", adc0);
        //    }
        //    break;
        //    case 1: {
        //        SSD1351_printf(COLOR_GREEN, small_font, "X: %u\n", adc1);
        //        SSD1351_printf(COLOR_PURPLE, small_font, "Y: %u\n", adc0);
        //    }
        //    break;
        //    case 2: {
        //        SSD1351_draw_sprite(0, y, &sprite0);
        //    }
        //    break;
        //
        //default:
        //    break;
        //}

    }
}
