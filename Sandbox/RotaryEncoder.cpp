#include "RotaryEncoder.h"

#include "pico/stdlib.h"
#include <stdio.h>

// https://github.com/buxtronix/arduino/blob/master/libraries/Rotary/Rotary.cpp

// Use the full-step state table (emits a code at 00 only)
#define R_START 0x0
#define R_CW_FINAL  0x1
#define R_CW_BEGIN  0x2
#define R_CW_NEXT   0x3
#define R_CCW_BEGIN 0x4
#define R_CCW_FINAL 0x5
#define R_CCW_NEXT  0x6

// Clockwise step.
#define DIR_CW 0x10
// Anti-clockwise step.
#define DIR_CCW 0x20

const unsigned char ttable[7][4] = {
  // R_START
  { R_START,    R_CW_BEGIN,  R_CCW_BEGIN, R_START },
  // R_CW_FINAL
  { R_CW_NEXT,  R_START,     R_CW_FINAL,  R_START | DIR_CW },
  // R_CW_BEGIN
  { R_CW_NEXT,  R_CW_BEGIN,  R_START,     R_START },
  // R_CW_NEXT
  { R_CW_NEXT,  R_CW_BEGIN,  R_CW_FINAL,  R_START },
  // R_CCW_BEGIN
  { R_CCW_NEXT, R_START,     R_CCW_BEGIN, R_START },
  // R_CCW_FINAL
  { R_CCW_NEXT, R_CCW_FINAL, R_START,     R_START | DIR_CCW },
  // R_CCW_NEXT
  { R_CCW_NEXT, R_CCW_FINAL, R_CCW_BEGIN, R_START },
};

RotaryEncoder::RotaryEncoder(int pin_clk, int pin_dt, int pin_button) :
    clk_pin(pin_clk), dt_pin(pin_dt), btn_pin(pin_button), state(0), buttonState(ButtonState::Up), lastReleasedTime(absolute_time_t())
{

}

void RotaryEncoder::init() {
    gpio_init(clk_pin);
    gpio_set_dir(clk_pin, GPIO_IN);
    gpio_pull_up(clk_pin);

    gpio_init(dt_pin);
    gpio_set_dir(dt_pin, GPIO_IN);
    gpio_pull_up(dt_pin);
    
    if (btn_pin != -1) {
        gpio_init(btn_pin);
        gpio_set_dir(btn_pin, GPIO_IN);
        gpio_pull_up(btn_pin);
    }
}

RotaryEncoder::~RotaryEncoder() = default;

int RotaryEncoder::process(){
    uint8_t pinState = ((gpio_get(dt_pin)) << 1) | gpio_get(clk_pin);
    state = ttable[state & 0x0F][pinState];
    uint8_t result = state & 0x30;
    if (result == 0x10) {
        return 1;
    }
    else if (result == 0x20) {
        return -1;
    }
    else {
        return 0;
    }
}

void RotaryEncoder::poll() {
    // Process button state
    if (btn_pin != -1) {
        uint8_t cState = (uint8_t)(buttonState);
        cState <<= 1;
        cState &= 0b10;
        cState = cState | (!gpio_get(btn_pin) ? 1 : 0);
        buttonState = (ButtonState)cState;

        if (buttonState == ButtonState::Released) {
            lastReleasedTime = get_absolute_time();
            printf("last released: %llu\n", to_us_since_boot(lastReleasedTime));
        }
    }
}

ButtonState RotaryEncoder::get_button() const{
    return buttonState;
}