# Raspberry Pi Messaround

> Note: SSD1351 based off the one found [here](https://github.com/Gecko05/SSD1351-Driver-Library)

> Note: IttyMath is a modified version from [my other repo](https://gitlab.com/SageMade/itty-math) in order to support the RISC architecture of the Pico Pi

This repo contains small projects making use of the Pico-Pi, as well as providing a place for me to store any libraries that I happen to make during my adventures

# Setup

Use of this project requires a working Pi toolchain, including the pico SDK, MINGW, CMAKE, and armcc. This takes a bit of work, but I followed [this](https://shawnhymel.com/2096/how-to-set-up-raspberry-pi-pico-c-c-toolchain-on-windows-with-vs-code/) guide, making changes where I had to (for instance, the MINGW installers just didn't work on windows 10, meaning I had to find and download compiled versions manually). One configured, there is a batch script to generate the projects and create the build results. The build results will be stored under the `build` folder, and to copy the firmware to the Pico Pi, you'll need to move the `.uf2` file to the Pi when it's in removable storage mode.

Pinouts are subject to change, especially in the **Sandbox** project, but stable APIs will include example diagrams.

# Current Projects:

## Sandbox
Contains a WIP SSD1351 driver based on the one linked above, as well as testing various input devices such as buttons, joysticks, and rotary encoders. This is essentially my "sanbox" project. 

The driver and renderer are inspired by the [AdaFruit GFX library](https://github.com/adafruit/Adafruit-GFX-Library), and is designed so as to allow displays to be swapped out without needing to re-write render code (note that display drivers still need to be written and the graphics currently is set up for 16 bit color depth displays). The SSD1351 driver itself is designed to be self-contained, allowing multiple OLEDs to be driven from a single RP2040 using it's multiple hardware SPI interfaces. Future work includes a HAL to allow the driver to be more portable accross boards.
